<!DOCTYPE html>
<html>
    <head>
        <title>Laravel</title>

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

<!-- *:style.css -->
<link rel="stylesheet" href="assets/css/style.css/style_0.css" />
<!-- endbuild -->
    </head>
    <body>
        <div class="container">
            <div class="content">
                <div class="title">Laravel 5</div>
            </div>
        </div>
    <!-- *:script.js -->
<script src="assets/js/script.js/script_0.js"></script>
<!-- endbuild -->
    </body>
</html>
