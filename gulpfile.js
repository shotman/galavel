// Include gulp
var gulp = require('gulp');
// Include Our Plugins
var browserSync = require('browser-sync').create();
var php = require("gulp-connect-php");
var run = require('gulp-run');

// Compile Our Sass
/*gulp.task('scss', function() {
    return gulp.src('resources/scss/styles.scss')
        .pipe(sass())
        .pipe(rename('style.css'))
        .pipe(gulp.dest('public/assets/css'));
});*/

gulp.task('serve', function() {

    // start the php server
    // make sure we use the public directory since this is Laravel
    php.server({
        base: './public'
    },function(){
    	browserSync.init({
      		proxy: '127.0.0.1:8000'
    	});
  });
    gulp.watch('resources/views/**/*.blade.php').on('change', function () {
      browserSync.reload();
  	});
  	gulp.watch('resources/**/*.scss').on('change', function () {
      run('gassetic build').exec();
      browserSync.reload();
  	});
    gulp.watch('resources/**/*.js').on('change', function () {
      run('gassetic build').exec();
      browserSync.reload();
    });
    gulp.watch('gassetic.yml').on('change', function () {
      run('gassetic build').exec();
      browserSync.reload();
    });
});

gulp.task('start',function(){
	run('gassetic build').exec();
});
gulp.task('default', ['start','serve']);
